import java.util.Random;

public class Mutation  {
	
	private static int minGeneValue=0;
	private static int maxGeneValue=1;
	private static double stdDev=1/6*(maxGeneValue-minGeneValue);
	
	public static String[] swapMutationFirstHalf(String[] arrayToMutate){
		String[] cloneArray = arrayToMutate.clone();
		Random rnd = new Random();
				
		int firstSwapIndex = rnd.nextInt(MainGA.firstHalf);
		int secondSwapIndex = rnd.nextInt(MainGA.firstHalf);
		while(firstSwapIndex==secondSwapIndex){
			secondSwapIndex = rnd.nextInt(MainGA.firstHalf);
		}
		
		String aCopy = cloneArray[firstSwapIndex];
		cloneArray[firstSwapIndex] = cloneArray[secondSwapIndex];
		cloneArray[secondSwapIndex] = aCopy;
		
		return cloneArray;
	}
	
	public static String[] swapMutationSecondHalf(String[] arrayToMutate){
		String[] cloneArray = arrayToMutate.clone();
		Random rnd = new Random();
		
		int firstSwapIndex = rnd.nextInt(MainGA.secondHalf)+MainGA.firstHalf;
		int secondSwapIndex = rnd.nextInt(MainGA.secondHalf)+MainGA.firstHalf;
		while(firstSwapIndex==secondSwapIndex){
			secondSwapIndex = rnd.nextInt(MainGA.secondHalf)+MainGA.firstHalf;
		}
		
		String aCopy = cloneArray[firstSwapIndex];
		cloneArray[firstSwapIndex] = cloneArray[secondSwapIndex];
		cloneArray[secondSwapIndex] = aCopy;
		
		return cloneArray;
	}

	public static double[] uniformMutation(double[] arrayToMutate){
		double[] cloneArray = arrayToMutate.clone();
		for(int i=0;i<MainGA.chromLength;i++){
		if(MainGA.mutRate>MainGA.randomDouble()){
			cloneArray[i]=MainGA.randomDouble();
		}
		}
		return cloneArray;
	}

	public static double[] gaussianMutation(double[] arrayToMutate){
		double[] cloneArray = arrayToMutate.clone();
		for(int i=0;i<MainGA.chromLength;i++){
			if(MainGA.mutRate>MainGA.randomDouble()){
			cloneArray[i]=gaussian(cloneArray[i]);
			cloneArray[i]=clamp(cloneArray[i],minGeneValue,maxGeneValue);
			}
		}
		return cloneArray;
	}
	
	public static double gaussian(double oldGene)
	{
		Random rnd = new Random();
	    double x1 = rnd.nextDouble();
	    double x2 = rnd.nextDouble();
	     
	    if(x1 == 0)
	        x1 = 1;
	    if(x2 == 0)
	        x2 = 1;
	 
	    double y1 = Math.sqrt(-2.0 * Math.log(x1)) * Math.cos(2.0 * Math.PI * x2);
	    return y1 * stdDev + oldGene;
	}
	 
	public static double clamp(double val, double min, double max)
	{
	    if (val >= max)
	        return max;
	    if (val <= min)
	        return min;
	    return val;
	}
	
}