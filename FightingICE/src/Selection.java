import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import org.apache.commons.io.FileUtils;

public class Selection {
	private static String directoryPath = "./log/point";
	public static int theGenerationResult[][]=new int[MainGA.popSize][5];
	
	public static void fitnessFunction() throws IOException {

		int gaScore[][] = new int[3 * MainGA.noMatch][3];
		int temp[] = new int[4];

		File dir = new File(directoryPath);
		File[] files = dir.listFiles();
		int count = 0;

		for (File file : files) {
			if (!file.exists()) {
				continue;
			} else if (file.isFile()) {
				try (BufferedReader br = new BufferedReader(new FileReader(file))) {
					String line;
					int j = count * 3;
					while ((line = br.readLine()) != null) {
						if (Character.isDigit(line.charAt(0)) && Character.isDigit(line.charAt(line.length() - 1))) {
							String[] parts = line.split(",");
							temp[0] = Integer.parseInt(parts[0]);
							temp[1] = Integer.parseInt(parts[1]);
							temp[2] = Integer.parseInt(parts[2]);
							// time not in use
							temp[3] = Integer.parseInt(parts[3]);
							gaScore[j] = temp.clone();
							j++;
						} else {
							count--;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			count++;
		}

		for (int i = 0; i < 3 * MainGA.noMatch; i++) {
			int score = 1000 * gaScore[i][1] / (gaScore[i][1] + gaScore[i][2]);
			int myHP = gaScore[i][1];
			int oppHP = gaScore[i][2];

			// temp file will delete during selection
			File thisGenerationScore = new File("thisGenerationScore.txt");
			try (FileWriter fw1 = new FileWriter(thisGenerationScore, true);
					BufferedWriter bw1 = new BufferedWriter(fw1);
					PrintWriter out1 = new PrintWriter(bw1);) {
				out1.println(Integer.toString(score) + "," + Integer.toString(myHP) + "," + Integer.toString(oppHP));
				out1.close();
			} catch (IOException e) {
				System.out.println("COULD NOT LOG!!");
			}

		}

		FileUtils.cleanDirectory(dir);

	}

	//sort score and keep best two to next generation
	public static void initSelection() {
		String path1 = "thisGenerationScore.txt";
		int resultDetail[][] = new int[3*MainGA.noMatch][3];
		int currentBest1[] = new int[4];
		int currentBest2[] = new int[4];
		int concludetemp[] = new int[4];
		int tempResult[] = new int[3];
		
		try (BufferedReader br = new BufferedReader(new FileReader(path1))) {
			String line;
			// j is number of rounds
			int j = 0;
			// used to index current AI
			int k = 0;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split("\\,");
				tempResult[0] = Integer.parseInt(parts[0]);
				tempResult[1] = Integer.parseInt(parts[1]);
				tempResult[2] = Integer.parseInt(parts[2]);

				resultDetail[j] = tempResult.clone();
				j++;
				if (j == 3 * MainGA.noMatch) {
					for (int i = 0; i < 3 * MainGA.noMatch; i++) {
						concludetemp[0] += resultDetail[i][0] ;
						concludetemp[1] += resultDetail[i][1] ;
						concludetemp[2] += resultDetail[i][2] ;
					}

					
					// index of which AI
					concludetemp[3] = k;
					//clone this result to whole result for future selection
					theGenerationResult[k]=concludetemp.clone();
					if (k == 0) {
						currentBest1 = concludetemp.clone();
					} else if (k == 1) {
						currentBest2 = concludetemp.clone();
					} else {
						// compare to keep only best two
						// if new chromosome better than both.
						if (concludetemp[0] >= currentBest1[0] && concludetemp[0] >= currentBest2[0]) {
							if (currentBest1[0] > currentBest2[0]) {
								currentBest2 = concludetemp.clone();
							} else if (currentBest1[0] < currentBest2[0]) {
								currentBest1 = concludetemp.clone();
							} else {
								// best1[0]==best2[0]
								if (currentBest1[1] > currentBest2[1]) {
									currentBest2 = concludetemp.clone();
								} else if (currentBest1[1] < currentBest2[1]) {
									currentBest1 = concludetemp.clone();
								} else {

									// [2] is opponent HP, the lower the better
									if (currentBest1[2] < currentBest2[2]) {
										currentBest2 = concludetemp.clone();
									} else if (currentBest1[2] > currentBest2[2]) {
										currentBest1 = concludetemp.clone();
									}
								}
							}
						} else if (concludetemp[0] > currentBest1[0] && concludetemp[0] <= currentBest2[0]) {
							currentBest1 = concludetemp.clone();
						} else if (concludetemp[0] <= currentBest1[0] && concludetemp[0] > currentBest2[0]) {
							currentBest2 = concludetemp.clone();
						}
					}
					j = 0;
					k++;
					for(int i=0;i<4;i++){
						concludetemp[i]=0;
					}
				}
			}
			
			// update the parents
			
			String tempAction[]=new String[MainGA.chromLength];
			double tempProb[]=new double[MainGA.chromLength];
			int tempOldResult[]=new int [3];
			
			//store the old best chromosome into the temp
			tempAction=MainGA.actNamePopulation[MainGA.testingPopSize].clone();
			tempProb=MainGA.ActProbPopulation[MainGA.testingPopSize].clone();
			//update the old best index to the new best soluiotn's index
			tempOldResult=theGenerationResult[MainGA.testingPopSize].clone();
			tempOldResult[3]=currentBest1[3] ;
			//update the best
			MainGA.bestActionChro1 = MainGA.actNamePopulation[currentBest1[3]].clone();
			MainGA.corresProbChro1 = MainGA.ActProbPopulation[currentBest1[3]].clone();
			MainGA.actNamePopulation[MainGA.testingPopSize] =MainGA.bestActionChro1.clone();
			MainGA.ActProbPopulation[MainGA.testingPopSize] =MainGA.corresProbChro1.clone();
			
			//store old best from temp to current best orinal position
			MainGA.actNamePopulation[currentBest1[3]]=tempAction.clone();
			MainGA.ActProbPopulation[currentBest1[3]]=tempProb.clone();				
			
			currentBest1[3]=MainGA.testingPopSize;
			MainGA.bestScore1 = currentBest1.clone();
			
			//store best at the end of the result 
			theGenerationResult[MainGA.testingPopSize]=currentBest1.clone();
			theGenerationResult[tempOldResult[3]]=tempOldResult.clone();
			
			String tempAction2[]=new String[MainGA.chromLength];
			double tempProb2[]=new double[MainGA.chromLength];
			int tempOldResult2[]=new int [3];
			
			
			//store the old best chromosome into the temp
			tempAction2=MainGA.actNamePopulation[MainGA.testingPopSize+1].clone();
			tempProb2=MainGA.ActProbPopulation[MainGA.testingPopSize+1].clone();
			//update the old best index to the new best soluiotn's index
			tempOldResult2=theGenerationResult[MainGA.testingPopSize+1].clone();
			tempOldResult2[3]=currentBest2[3] ;
			//update the best
			MainGA.bestActionChro2 = MainGA.actNamePopulation[currentBest2[3]].clone();
			MainGA.corresProbChro2 = MainGA.ActProbPopulation[currentBest2[3]].clone();
			MainGA.actNamePopulation[MainGA.testingPopSize+1] =MainGA.bestActionChro2.clone();
			MainGA.ActProbPopulation[MainGA.testingPopSize+1] =MainGA.corresProbChro2.clone();
			
			//store old best from temp to current best orinal position
			MainGA.actNamePopulation[currentBest2[3]]=tempAction2.clone();
			MainGA.ActProbPopulation[currentBest2[3]]=tempProb2.clone();				
			
			currentBest2[3]=MainGA.testingPopSize+1;
			MainGA.bestScore2 = currentBest2.clone();
			
			//store best at the end of the result 
			theGenerationResult[MainGA.testingPopSize+1]=currentBest2.clone();
			theGenerationResult[tempOldResult2[3]]=tempOldResult2.clone();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(MainGA.bestScore1[0] + "," + MainGA.bestScore2[0]);	

		File bestChromosome1 = new File("bestChromosome1.txt");
		try (FileWriter fw = new FileWriter(bestChromosome1, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw);) {

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.bestActionChro1[i]);
			}

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.corresProbChro1[i]);
			}

			out.close();
		} catch (IOException e) {
			System.out.println("COULD NOT LOG!!");
		}

		File bestChromosome2 = new File("bestChromosome2.txt");
		try (FileWriter fw = new FileWriter(bestChromosome2, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw);) {

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.bestActionChro2[i]);
			}

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.corresProbChro2[i]);
			}

			out.close();
		} catch (IOException e) {
			System.out.println("COULD NOT LOG!!");
		}

		File f = null;
		boolean bool = false;
		try {
			f = new File("thisGenerationScore.txt");
			bool = f.delete();
			f.createNewFile();
			bool = f.delete();

		} catch (Exception e) {
			// if any error occurs
			e.printStackTrace();
		}
		
	}

	
	//sort score and keep best two to next generation
	public static void generationSelection() {
		String path1 = "thisGenerationScore.txt";
		int resultDetail[][] = new int[3 * MainGA.noMatch][3];
		int currentBest1[] = new int[4];
		int currentBest2[] = new int[4];
		// copy the best two so far
		currentBest1 = MainGA.bestScore1.clone();
		currentBest2 = MainGA.bestScore2.clone();
		int concludetemp[] = new int[4];
		int tempResult[] = new int[3];

		try (BufferedReader br = new BufferedReader(new FileReader(path1))) {
			String line;
			// j is number of rounds
			int j = 0;
			// used to index current AI
			int k = 0;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split("\\,");
				tempResult[0] = Integer.parseInt(parts[0]);
				tempResult[1] = Integer.parseInt(parts[1]);
				tempResult[2] = Integer.parseInt(parts[2]);
				resultDetail[j] = tempResult.clone();
				j++;

				// one AI finished, conclude the data
				// compare with parents
				if (j == 3 * MainGA.noMatch) {
					for (int i = 0; i < 3 * MainGA.noMatch; i++) {
						concludetemp[0] = resultDetail[i][0] + concludetemp[0];
						concludetemp[1] = resultDetail[i][1] + concludetemp[1];
						concludetemp[2] = resultDetail[i][2] + concludetemp[2];
					}
					
					// index of which AI
					concludetemp[3] = k;

					theGenerationResult[k]=concludetemp.clone();
					
					// compare to keep only best two
					// if new chromosome better than both.
					if (concludetemp[0] > currentBest1[0] && concludetemp[0] > currentBest2[0]) {
						if (currentBest1[0] > currentBest2[0]) {
							currentBest2 = concludetemp.clone();
						} else if (currentBest1[0] < currentBest2[0]) {
							currentBest1 = concludetemp.clone();
						} else {
							// best1[0]==best2[0]
							if (currentBest1[1] > currentBest2[1]) {
								currentBest2 = concludetemp.clone();
							} else if (currentBest1[1] < currentBest2[1]) {
								currentBest1 = concludetemp.clone();
							} else {

								// [2] is opponent HP, the lower the better
								if (currentBest1[2] < currentBest2[2]) {
									currentBest2 = concludetemp.clone();
								} else if (currentBest1[2] > currentBest2[2]) {
									currentBest1 = concludetemp.clone();
								}
							}
						}
					} else if (concludetemp[0] > currentBest1[0] && concludetemp[0] <= currentBest2[0]) {
						currentBest1 = concludetemp.clone();
					} else if (concludetemp[0] <= currentBest1[0] && concludetemp[0] > currentBest2[0]) {
						currentBest2 = concludetemp.clone();
					}else if (concludetemp[0] == currentBest1[0] && concludetemp[0] == currentBest2[0]){
						if (currentBest1[1] >= concludetemp[1]&&concludetemp[1]>currentBest2[1]) {
							currentBest2 = concludetemp.clone();
						} else if (currentBest1[1]<concludetemp[1]&&concludetemp[1] <= currentBest2[1]) {
							currentBest1 = concludetemp.clone();
						} else if (currentBest1[1]<concludetemp[1]&&concludetemp[1] > currentBest2[1]) {
							// [2] is opponent HP, the lower the better
							if (currentBest1[2] < currentBest2[2]) {
								currentBest2 = concludetemp.clone();
							} else if (currentBest1[2] > currentBest2[2]) {
								currentBest1 = concludetemp.clone();
							} 
						}  
						else {

							// [2] is opponent HP, the lower the better
							if (currentBest1[2] <=concludetemp[2]&&concludetemp[2]< currentBest2[2]) {
								currentBest2 = concludetemp.clone();
							} else if (currentBest1[2] >concludetemp[2]&&concludetemp[2]>= currentBest2[2]) {
								currentBest1 = concludetemp.clone();
							}
						}
					}

					j = 0;
					k++;
					for(int i=0;i<4;i++){
						concludetemp[i]=0;
					}
				}
			}

			// update the parents
			if (currentBest1[3] != MainGA.testingPopSize) {
				
				String tempAction[]=new String[MainGA.chromLength];
				double tempProb[]=new double[MainGA.chromLength];
				int tempOldResult[]=new int [5];
				
				//store the old best chromosome into the temp
				tempAction=MainGA.actNamePopulation[MainGA.testingPopSize].clone();
				tempProb=MainGA.ActProbPopulation[MainGA.testingPopSize].clone();
				//update the old best index to the new best soluiotn's index
				tempOldResult=theGenerationResult[MainGA.testingPopSize].clone();
				tempOldResult[3]=currentBest1[3] ;
				//update the best
				MainGA.bestActionChro1 = MainGA.actNamePopulation[currentBest1[3]].clone();
				MainGA.corresProbChro1 = MainGA.ActProbPopulation[currentBest1[3]].clone();
				MainGA.actNamePopulation[MainGA.testingPopSize] =MainGA.bestActionChro1.clone();
				MainGA.ActProbPopulation[MainGA.testingPopSize] =MainGA.corresProbChro1.clone();
				
				//store old best from temp to current best orinal position
				MainGA.actNamePopulation[currentBest1[3]]=tempAction.clone();
				MainGA.ActProbPopulation[currentBest1[3]]=tempProb.clone();				
				
				currentBest1[3]=MainGA.testingPopSize;
				MainGA.bestScore1 = currentBest1.clone();
				
				//store best at the end of the result 
				theGenerationResult[MainGA.testingPopSize]=currentBest1.clone();
				theGenerationResult[tempOldResult[3]]=tempOldResult.clone();
			}

			if (currentBest2[3] != MainGA.testingPopSize + 1) {
				
				String tempAction[]=new String[MainGA.chromLength];
				double tempProb[]=new double[MainGA.chromLength];
				int tempOldResult[]=new int [5];
				
				
				//store the old best chromosome into the temp
				tempAction=MainGA.actNamePopulation[MainGA.testingPopSize+1].clone();
				tempProb=MainGA.ActProbPopulation[MainGA.testingPopSize+1].clone();
				//update the old best index to the new best soluiotn's index
				tempOldResult=theGenerationResult[MainGA.testingPopSize+1].clone();
				tempOldResult[3]=currentBest2[3] ;
				//update the best
				MainGA.bestActionChro2 = MainGA.actNamePopulation[currentBest2[3]].clone();
				MainGA.corresProbChro2 = MainGA.ActProbPopulation[currentBest2[3]].clone();
				MainGA.actNamePopulation[MainGA.testingPopSize+1] =MainGA.bestActionChro2.clone();
				MainGA.ActProbPopulation[MainGA.testingPopSize+1] =MainGA.corresProbChro2.clone();
				
				//store old best from temp to current best orinal position
				MainGA.actNamePopulation[currentBest2[3]]=tempAction.clone();
				MainGA.ActProbPopulation[currentBest2[3]]=tempProb.clone();				
				
				currentBest2[3]=MainGA.testingPopSize+1;
				MainGA.bestScore2 = currentBest2.clone();
				
				//store best at the end of the result 
				theGenerationResult[MainGA.testingPopSize+1]=currentBest2.clone();
				theGenerationResult[tempOldResult[3]]=tempOldResult.clone();
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		File f = null;
		boolean bool = false;
		try {
			f = new File("thisGenerationScore.txt");
			bool = f.delete();
			f.createNewFile();
			bool = f.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}

		File f1 = null;
		boolean bool1 = false;
		try {
			f1 = new File("bestChromosome1.txt");
			bool1 = f1.delete();
			f1.createNewFile();
			bool1 = f1.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}

		File f2 = null;
		boolean bool2 = false;
		try {
			f2 = new File("bestChromosome2.txt");
			bool2 = f2.delete();
			f2.createNewFile();
			bool2 = f2.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}

		File bestChromosome1 = new File("bestChromosome1.txt");
		try (FileWriter fw = new FileWriter(bestChromosome1, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw);) {

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.bestActionChro1[i]);
			}

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.corresProbChro1[i]);
			}

			out.close();
		} catch (IOException e) {
			System.out.println("COULD NOT LOG!!");
		}

		File bestChromosome2 = new File("bestChromosome2.txt");
		try (FileWriter fw = new FileWriter(bestChromosome2, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw);) {

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.bestActionChro2[i]);
			}

			for (int i = 0; i < MainGA.chromLength; i++) {
				out.println(MainGA.corresProbChro2[i]);
			}

			out.close();
		} catch (IOException e) {
			System.out.println("COULD NOT LOG!!");
		}

		System.out.println(MainGA.bestScore1[0] + "," + MainGA.bestScore2[0]);		
	}
	
	public static int selectRouletteWheel() {
		
		//basic idea inspired by tutorialpoint GA
		//However not there is no code, it is easy to implement this function.
		//simply sum up all fitness to get a "pie"
		//random a number as fixed point
		//one it over this fixed point, we stop and take that index value
		
		int totalSum = 0;
		for (int x=0; x <MainGA.popSize  ; x++) {
			totalSum += theGenerationResult[x][0];
		}

		int rand = randomNumber(0,totalSum);
		int tempSum = 0;
		for (int x=0; x <MainGA.popSize  ; x++) {
			tempSum += theGenerationResult[x][0];
			if (tempSum >= rand) {  
				return x; 
			}
		}
		return -1;
	}
	
	//return an int
	public static int randomNumber(int min , int max) {
		Random r = new Random();
		double d = min + r.nextDouble() * (max - min);
		return (int)d;
	}
}