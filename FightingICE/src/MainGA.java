import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class MainGA {
		
		//population size
		public static int popSize = 10;
		
		//2 of the parents are not going to test again
		public static int testingPopSize=8;
		public static int noMatch=2;
		
		//chromosome/solution/genotype 
		public static int chromLength = 31;
		
		//cross over rate
		static float XOverRate=3/4;
		public static int firstHalf=14;
		public static int secondHalf=17;
		
		//mutation rate, change it have a play
		public static double mutRate = 0.15;
		private static double mutRateFirstHalf = 0.15;
		private static double mutRateSecondHalf = 0.15;
		
		//how many tournaments should be played
		private static int numberGeneration = 5000;
		private static int currentGenerationNo=0; 
		private static int scoreRequirement=3000;

		public static String bestActionChro1[];
		public static String bestActionChro2[];
		public static double corresProbChro1[];
		public static double corresProbChro2[];
		public static String selectedActionChro1[];
		public static String selectedActionChro2[];
		public static double selectedProbChro1[];
		public static double selectedProbChro2[];
		public static int bestScore1[]=new int[4];
		public static int bestScore2[]=new int[4];
		
		//the chromosomes array, 30 members, 10 line segments each
		public static String[][] actNamePopulation = new String[popSize][chromLength];
		public static double[][] ActProbPopulation = new double[popSize][chromLength];
		static String[] initChromosome = {"THROW_A","THROW_B","STAND_A","STAND_B","CROUCH_A","CROUCH_B","STAND_FA","STAND_FB","CROUCH_FA","CROUCH_FB"
				,"STAND_D_DF_FA","STAND_D_DF_FB","STAND_D_DF_FC","STAND_F_D_DFA","STAND_F_D_DFB","STAND_D_DB_BA","STAND_D_DB_BB","AIR_A","AIR_B"
				, "AIR_DA","AIR_DB","AIR_FA","AIR_FB","AIR_UA","AIR_UB","AIR_D_DF_FA","AIR_D_DF_FB","AIR_F_D_DFA","AIR_F_D_DFB",
				"AIR_D_DB_BA","AIR_D_DB_BB"};
		
		static double[] initProb ={0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2};

		
		public static void main(String[] args) throws IOException{
			runGA();
		}
		
		public static void runGA() throws IOException
		{
			//initialise the population
			initPopulation();
			//evaluation
			for(int i=0;i<popSize;i++){
							
				Process process = Runtime.getRuntime().exec("cmd /C start /wait   C:/Users/Wenger95/Documents/kclaiproject/FightingICE/WengerGA"+i+".bat");
							
				try {
					process.waitFor();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				Selection.fitnessFunction();
			}
			
			//call the function update the parents
			Selection.initSelection();
			
			for (int i=0;i<popSize;i++){		
				File f = null;
				 boolean bool = false;
			      try {
			         f = new File("AttackChromosome"+i+".txt");
			         bool = f.delete();
			         f.createNewFile();
			         bool = f.delete();
			      } catch(Exception e) {
			         // if any error occurs
			         e.printStackTrace();
			      }
			}
			
			for (int i=0;i<popSize;i++){		
				File f = null;
				 boolean bool = false;
			      try {
			         f = new File("ProbChromosome"+i+".txt");
			         bool = f.delete();
			         f.createNewFile();
			         bool = f.delete();
			      } catch(Exception e) {
			         e.printStackTrace();
			      }
			}
			
			//start a generation
			while((bestScore1[0]<scoreRequirement*noMatch&&bestScore2[0]<scoreRequirement*noMatch)&&currentGenerationNo<numberGeneration){
				//population -2 as 2 is parents	
				String[][] newActionPopulation = new String[testingPopSize][chromLength];	
				double[][] newCorresProb=new double[testingPopSize][chromLength];
				
				for (int i = 0 ; i < testingPopSize; i++) {
					//select parents
					
					int selectedP1=-1;
					int selectedP2=-1;
					//add call of 	public static int selectMemberUsingRouletteWheel() 
					do {
						selectedP1 = Selection.selectRouletteWheel();
						selectedP2 =  Selection.selectRouletteWheel(); 
					} while(selectedP1==selectedP2);
					
					selectedActionChro1=actNamePopulation[selectedP1];
					selectedActionChro2=actNamePopulation[selectedP2];
					selectedProbChro1=ActProbPopulation[selectedP1];
					selectedProbChro2=ActProbPopulation[selectedP2];
					
					//cross over
					if(randomDouble()<XOverRate){
						newActionPopulation[i]=Crossover.UniformXOver(selectedActionChro1, selectedActionChro2);
						newCorresProb[i]=Crossover.copyXOver(selectedProbChro1, selectedProbChro2);
					}
					else{
						if(randomDouble()<0.5){
							newActionPopulation[i]=selectedActionChro1.clone();
							newCorresProb[i]=selectedProbChro1.clone();
						}
						else{
							newActionPopulation[i]=selectedActionChro2.clone();
							newCorresProb[i]=selectedProbChro2.clone();
						}
					}
					//mutation
					newCorresProb[i] = Mutation.uniformMutation(newCorresProb[i]);					   
					//newCorresProb[i] = Mutation.gaussianMutation(newCorresProb[i]);
					
					if (randomDouble() < mutRateFirstHalf ){
						newActionPopulation[i] = Mutation.swapMutationFirstHalf(newActionPopulation[i]);					   
					}
					if (randomDouble() < mutRateSecondHalf ){
						newActionPopulation[i] = Mutation.swapMutationSecondHalf(newActionPopulation[i]);					   
					}
					
					
					File AttackChromosome = new File("AttackChromosome"+i+".txt");
					try (FileWriter fw =new FileWriter(AttackChromosome,true);
							BufferedWriter bw =new BufferedWriter(fw);
							PrintWriter out = new PrintWriter(bw))
					{
						for(int j=0;j<31;j++){
						out.println(newActionPopulation[i][j]);
						}
						out.close();
					} catch (IOException e) {
						System.out.println("COULD NOT LOG!!");
					}
					
					File ProbChromosome = new File("ProbChromosome"+i+".txt");
					try (FileWriter fw1 =new FileWriter(ProbChromosome,true);
							BufferedWriter bw1 =new BufferedWriter(fw1);
							PrintWriter out1 = new PrintWriter(bw1))
					{
						for(int j=0;j<31;j++){
						out1.println(newCorresProb[i][j]);
						}
						out1.close();
					} catch (IOException e) {
						System.out.println("COULD NOT LOG!!");
					}
					//output the chromosome and prob to file
					//WengerGA Ai file should read this file
				}
						
				for(int i=0;i<testingPopSize;i++){
					
					Process process = Runtime.getRuntime().exec("cmd /C start /wait   C:/Users/Wenger95/Documents/kclaiproject/FightingICE/WengerGA"+i+".bat");
					try {
						process.waitFor();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Selection.fitnessFunction();
				}
				
				for(int i=0;i<testingPopSize;i++){
					actNamePopulation[i]=newActionPopulation[i];
					ActProbPopulation[i]=newCorresProb[i];
				}
				
				//according to the score, bothHP get the best two
				Selection.generationSelection();
				
				for (int i=0;i<testingPopSize;i++){		
					File f = null;
					 boolean bool = false;
				      try {
				         f = new File("AttackChromosome"+i+".txt");
				         bool = f.delete();
				         f.createNewFile();
				         bool = f.delete();
				      } catch(Exception e) {
				         e.printStackTrace();
				      }
				}
				
				for (int i=0;i<testingPopSize;i++){		
					File f = null;
					 boolean bool = false;
				      try {
				         f = new File("ProbChromosome"+i+".txt");
				         bool = f.delete();
				         f.createNewFile();
				         bool = f.delete();
				      } catch(Exception e) {
				         e.printStackTrace();
				      }
				}
				
				currentGenerationNo++;
			}
		}
		
		//Initialises the population
		private static void initPopulation()
		{
			for (int i = 0; i < popSize; i++)
			{
				if(i==0){
					//this initChromosome is our benchmark
					//delete 0
					actNamePopulation[0]=initChromosome.clone();
					ActProbPopulation[0]=initProb.clone();
				}
				else {
				actNamePopulation[i]=shiftArray(initChromosome);
				ActProbPopulation[i]=allRandom(initProb);
				}
				File AttackChromosome = new File("AttackChromosome"+i+".txt");
				try (FileWriter fw =new FileWriter(AttackChromosome,true);
						BufferedWriter bw =new BufferedWriter(fw);
						PrintWriter out = new PrintWriter(bw))
				{
					for(int j=0;j<31;j++){
					out.println(actNamePopulation[i][j]);
					}
					out.close();
				} catch (IOException e) {
					System.out.println("COULD NOT LOG!!");
				}
				
				File ProbChromosome = new File("ProbChromosome"+i+".txt");
				try (FileWriter fw1 =new FileWriter(ProbChromosome,true);
						BufferedWriter bw1 =new BufferedWriter(fw1);
						PrintWriter out1 = new PrintWriter(bw1))
				{
					for(int j=0;j<31;j++){
					out1.println(ActProbPopulation[i][j]);
					}
					out1.close();
				} catch (IOException e) {
					System.out.println("COULD NOT LOG!!");
				}
			}
		}
		
		static String[] shiftArray(String[] initArray)
		{
			String[] cloneArray = initArray.clone();
			Random rnd = new Random();
			//shift based on random value
			int firstShift = rnd.nextInt(firstHalf);
			for (int i = 0; i < firstHalf; i++)
			{
				int exchangeIndex=firstShift+i;
				if(exchangeIndex>firstHalf)
				{
					exchangeIndex=exchangeIndex-firstHalf;
				}
				String aCopy = cloneArray[exchangeIndex];
				cloneArray[exchangeIndex] = cloneArray[i];
				cloneArray[i] = aCopy;
			}
			
			int secondShift = rnd.nextInt(secondHalf);
			for(int i=firstHalf;i<cloneArray.length;i++)
			{
				int exchangeIndex=secondShift+i;
				if(exchangeIndex>cloneArray.length-1)
				{
					exchangeIndex=exchangeIndex-chromLength+firstHalf-1;
				}
				String aCopy = cloneArray[exchangeIndex];
				cloneArray[exchangeIndex] = cloneArray[i];
				cloneArray[i] = aCopy;
			}
			return cloneArray;
		}
		
		public static double[] allRandom(double[] arrayToMutate){
			double[] cloneArray = arrayToMutate.clone();
			for(int i=0;i<chromLength;i++){
			cloneArray[i]=randomDouble();
			}
			return cloneArray;
		}
		
		//Returns a random number n 0.0 <= n <= 1.0
		static double randomDouble()
		{
			Random r = new Random();
			return r.nextInt(1000) / 1000.0;
		}
		
	
}
