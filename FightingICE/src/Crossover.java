
public class Crossover {

	static int option[]=new int[MainGA.chromLength]; 
	public static String[] UniformXOver(String[] parent1, String[] parent2){
		
		String[] child=new String [MainGA.chromLength];
		
		for(int i =0; i<MainGA.chromLength;i++){
			if(MainGA.randomDouble()<0.5){
				child[i]=parent1[i];
				//store the choice
				option[i]=1;
			}else{
				child[i]=parent2[i];
				option[i]=2;
			}
		}
		return child;
	}
	
	public static double[] copyXOver(double[] parent1, double[] parent2){
		
		double[] child=new double [MainGA.chromLength];
		
		for(int i =0; i<MainGA.chromLength;i++){
			if(option[i]==1){
				//copy the value based on previous choice
				child[i]=parent1[i];
			}else{
				child[i]=parent2[i];
			}
		}
		return child;
	}
}
