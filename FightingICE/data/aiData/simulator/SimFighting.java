package simulator;

import java.util.Deque;
import java.util.LinkedList;
import java.util.logging.Level;

import logSystem.LogSystem;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.KeyData;
import enumerate.Action;
import enumerate.State;
import fighting.Attack;
import simulator.SimCharacter;
import simulator.SimAttack;

public class SimFighting {

	public final static int SIMULATE_LIMIT = 60;
	private int stageMaxX;
	private SimCharacter playerOneCharacter;
	private SimCharacter playerTwoCharacter;
	private Deque<SimAttack> attackDeque;
	private Deque<Action> oneInputAction;
	private Deque<Action> twoInputAction;
	
	private int round;
	
	private boolean player;
	
	public SimFighting(GameData gameData, FrameData frameData, boolean player, Deque<Action> myAct, Deque<Action> oppAct) {
		
		this.player = player;
		
		playerOneCharacter = new SimCharacter(frameData.getP1(),gameData.getPlayerOneMotion(),player);
		playerTwoCharacter = new SimCharacter(frameData.getP2(),gameData.getPlayerTwoMotion(),player);
		
		Deque<SimAttack> simAttack = new LinkedList<SimAttack>();
		int size = frameData.getAttack().size();
		for(int i = 0 ; i < size ; i++){
			Attack temp = frameData.getAttack().pop();
			SimAttack tempSimAttack = new SimAttack(temp);
			simAttack.addLast(tempSimAttack);
			frameData.getAttack().add(temp);
		}
		
		this.attackDeque = simAttack;
		
		this.oneInputAction = (player)? myAct:oppAct;
		this.twoInputAction = (!player)? myAct:oppAct;
		
		stageMaxX = 960;
		
		round = frameData.getRound();
	}

	public FrameData getFrameData(long remainingTime, KeyData keyData){
		CharacterData p1 = new CharacterData(playerOneCharacter);
		CharacterData p2 = new CharacterData(playerTwoCharacter);
		
		Deque<Attack> attacks = new LinkedList<Attack>();
		int size = attackDeque.size();
		for(int i = 0 ; i < size ; i++){
			SimAttack temp = attackDeque.pop();
			Attack tempSimAttack = new Attack(temp);
			attacks.addLast(tempSimAttack);
			attackDeque.add(temp);
		}
		
		FrameData frameData = new FrameData(p1, p2, remainingTime, round, attacks, keyData);
		return frameData;
	}
	
	public void processingFight(){
		
		updateCharacter();
		if(	this.oneInputAction != null &&
			!this.oneInputAction.isEmpty() && 
			ableAction(playerOneCharacter, this.oneInputAction.getFirst()) && 
			isValidAction(playerOneCharacter, this.oneInputAction.getFirst())){
			playerOneCharacter.runMotion(this.oneInputAction.pop());
		}
		
		if(this.twoInputAction != null &&
		   !this.twoInputAction.isEmpty() && 
		   ableAction(playerTwoCharacter, this.twoInputAction.getFirst()) && 
		   isValidAction(playerTwoCharacter, this.twoInputAction.getFirst()))
			playerTwoCharacter.runMotion(this.twoInputAction.pop());
		
		calculationAttackParameter();

		calculationHit();
		
	}
	
	private boolean isValidAction(SimCharacter character, Action action) {
		if (character.getState() == State.AIR){
			return action.name().contains("AIR");
		}
		else 	
			return !action.name().contains("AIR");
	}

	/**
	 * This method calculates the parameters of Attack objects.
	 */
	private void calculationAttackParameter(){
		// update the coordinates of Attacks (short distance)
		if(playerOneCharacter.getAttack() != null) {
			if(!playerOneCharacter.getAttack().update(playerOneCharacter)) playerOneCharacter.destroyAttackInstance();
		}
		if(playerTwoCharacter.getAttack() != null) {
			if(!playerTwoCharacter.getAttack().update(playerTwoCharacter)) playerTwoCharacter.destroyAttackInstance();
		}
		
		// update the coordinates of Attacks (long distance)
		for(int i = 0 ; i < attackDeque.size() ; i++){
			// if attack's nowFrame reach end of duration, remove it.
			if(attackDeque.getFirst().update()){
				attackDeque.addLast(attackDeque.removeFirst());
			}
			else attackDeque.removeFirst();
		}		
	}

	/**
	 * This method calculates the collision effects.
	 */
	private void calculationHit(){
		boolean p1AttackCheck = false;
		boolean p2AttackCheck = false;
		SimAttack p1Attack, p2Attack;
		
		p1Attack = new SimAttack(playerOneCharacter.getAttack());
		p2Attack = new SimAttack(playerTwoCharacter.getAttack());
		
		// long distance attack
				// Check an element in attackDeque one by one whether or not there is a hit between the character and attack object.  When there is a hit, perform the corresponding process accordingly. 
				for(int i = 0 ; i < attackDeque.size() ; i++){
					// attack = 2P suffer = 1P
					if(!attackDeque.getFirst().isPlayerNumber())
					{
						// if P2's attack hits P1 character, run a guard or hit motion and remove this attack. 
						if(detectionHit(playerOneCharacter , attackDeque.getFirst())) playerOneCharacter.hitAttackObject(playerTwoCharacter, attackDeque.removeFirst());
						else attackDeque.addLast(attackDeque.removeFirst());
					}
					// attack = 1P suffer = 2P
					else
					{
						// if P1's attack hits P2 character, run a guard or hit motion and remove this attack. 
						if(detectionHit(playerTwoCharacter , attackDeque.getFirst())) playerTwoCharacter.hitAttackObject(playerOneCharacter, attackDeque.removeFirst());
						else attackDeque.addLast(attackDeque.removeFirst());
					}
				}	

				// short distance attack
				// Is an attack present?
				if(playerOneCharacter.getAttack() != null){
					if(detectionHit(playerTwoCharacter , playerOneCharacter.getAttack()))
					{
						p1AttackCheck = true; 
					}
				}

				if(playerTwoCharacter.getAttack() != null){
					if(detectionHit(playerOneCharacter , playerTwoCharacter.getAttack()))
					{
						p2AttackCheck = true; 
					}
				}
				
				if(p1AttackCheck){
					// if P2's attack hits P1 character, run a guard or hit motion and remove this attack. 
					playerTwoCharacter.hitAttackObject(playerOneCharacter,p1Attack);
					// run effect of attacker
					playerOneCharacter.hitAttack();
				}
				
				if(p2AttackCheck){
					// if P1's attack hits P2 character, run a guard or hit motion and remove this attack. 
					playerOneCharacter.hitAttackObject(playerTwoCharacter,p2Attack);
					// run effect of attacker
					playerTwoCharacter.hitAttack();
				}
	}
	
	/**
	 * Calculate collision.
	 * @param characterObj
	 * @param attackOjb
	 * @return hit or not
	 */
	private boolean detectionHit(SimCharacter characterObj, SimAttack attackOjb){
		if(characterObj.getState() == State.DOWN){
			return false;
		}else if(characterObj.getHitAreaL() < attackOjb.getHitAreaNow().getR() && characterObj.getHitAreaR() > attackOjb.getHitAreaNow().getL() && characterObj.getHitAreaT() < attackOjb.getHitAreaNow().getB() && characterObj.getHitAreaB() > attackOjb.getHitAreaNow().getT()){
			SimCharacter myCharacter = (this.player)? playerOneCharacter:playerTwoCharacter;
			
			LogSystem.getInstance().logger.log(Level.FINE, "My character (L, R, T, B): (" + myCharacter.getHitAreaL() + ", " + myCharacter.getHitAreaR() + ", " + myCharacter.getHitAreaT() + ", " + myCharacter.getHitAreaB() + ")" +
							   "\tOpp character (L, R, T, B): (" + characterObj.getHitAreaL() + ", " + characterObj.getHitAreaR() + ", " + characterObj.getHitAreaT() + ", " + characterObj.getHitAreaB() + ")" + 
							   "\tHitArea (L, R, T, B): (" + attackOjb.getHitAreaNow().getL() + ", " + attackOjb.getHitAreaNow().getR() + ", " + attackOjb.getHitAreaNow().getT() + ", " + attackOjb.getHitAreaNow().getB() + ")");
			return true;
		}else if(characterObj.getHitAreaL() < attackOjb.getHitAreaNow().getR() && characterObj.getHitAreaT() < attackOjb.getHitAreaNow().getB() && characterObj.getHitAreaR() > attackOjb.getHitAreaNow().getL() && characterObj.getHitAreaB() > attackOjb.getHitAreaNow().getT()){
			SimCharacter myCharacter = (this.player)? playerOneCharacter:playerTwoCharacter;
			
			LogSystem.getInstance().logger.log(Level.FINE, "My character (L, R, T, B): (" + myCharacter.getHitAreaL() + ", " + myCharacter.getHitAreaR() + ", " + myCharacter.getHitAreaT() + ", " + myCharacter.getHitAreaB() + ")" +
					   "\tOpp character (L, R, T, B): (" + characterObj.getHitAreaL() + ", " + characterObj.getHitAreaR() + ", " + characterObj.getHitAreaT() + ", " + characterObj.getHitAreaB() + ")" + 
					   "\tHitArea (L, R, T, B): (" + attackOjb.getHitAreaNow().getL() + ", " + attackOjb.getHitAreaNow().getR() + ", " + attackOjb.getHitAreaNow().getT() + ", " + attackOjb.getHitAreaNow().getB() + ")");
			return true;
		}else{
			return false;
		}	
	}
	
	/**
	 * @param character
	 * @param action
	 * @return Character is able to act or not.
	 */
	private boolean ableAction(SimCharacter character, Action action){
		boolean checkFrame = (character.getMotionVector().elementAt(character.getAction().ordinal()).getCancelAbleFrame() <= character.getMotionVector().elementAt(character.getAction().ordinal()).getFrameNumber()-character.getRemainingFrame());

		boolean checkAction	= (character.getMotionVector().elementAt(character.getAction().ordinal()).getCancelAbleMotionLevel()>=character.getMotionVector().elementAt(action.ordinal()).getMotionLevel());
		
		if(character.getEnergy() < -character.getMotionVector().elementAt(action.ordinal()).getAttackStartAddEnergy()) return false;
		
		if(character.isControl())
		{
			return true;
		}else if((character.isHitConfirm() && checkFrame && checkAction )){
			return true;
		}
		else return false;
	}

	/**
	 * Update character's parameters
	 */
	private void updateCharacter(){
		// update each character.
		playerOneCharacter.update();
		playerTwoCharacter.update();

		// enque object attack if the data is missile decision
		if(playerOneCharacter.getAttack() != null) {
			if(playerOneCharacter.getAttack().checkProjectile()){
				SimAttack obj = new SimAttack(playerOneCharacter.getAttack());
				attackDeque.addLast(obj);
				playerOneCharacter.destroyAttackInstance();
			}
		}
		if(playerTwoCharacter.getAttack() != null) {
			if(playerTwoCharacter.getAttack().checkProjectile()){
				SimAttack obj = new SimAttack(playerTwoCharacter.getAttack());
				attackDeque.addLast(obj);
				playerTwoCharacter.destroyAttackInstance();
			}
		}

		// change player's direction
		if(playerOneCharacter.isControl())
			playerOneCharacter.frontDecision(	playerOneCharacter.getHitAreaL()+(playerOneCharacter.getHitAreaR()-playerOneCharacter.getHitAreaL())/2,
												playerTwoCharacter.getHitAreaL()+(playerTwoCharacter.getHitAreaR()-playerTwoCharacter.getHitAreaL())/2);
		
		if(playerTwoCharacter.isControl())
			playerTwoCharacter.frontDecision(	playerTwoCharacter.getHitAreaL()+(playerTwoCharacter.getHitAreaR()-playerTwoCharacter.getHitAreaL())/2,
												playerOneCharacter.getHitAreaL()+(playerOneCharacter.getHitAreaR()-playerOneCharacter.getHitAreaL())/2);

		// run pushing effect
		detectionPush(playerOneCharacter,playerTwoCharacter);
		// run collision of the first and second character.
		detectionFusion(playerOneCharacter,playerTwoCharacter);
		// run an effect when both characters are in an edge of the stage.
		decisionEndStage();
	}
	
	/**
	 * Characters push each other
	 * @param Player1
	 * @param Player2
	 */
	private void detectionPush(SimCharacter Player1, SimCharacter Player2){
		// whether P1 and P2 overlap (collide) or not?
		if(Player1.getHitAreaL() < Player2.getHitAreaR() && Player1.getHitAreaT() < Player2.getHitAreaB() && Player1.getHitAreaR() > Player2.getHitAreaL() && Player1.getHitAreaB() > Player2.getHitAreaT()){
			
			if(Player1.isFront()){
				if(Player1.getSpeedX() > -Player2.getSpeedX()){
					Player2.moveX(pushMovement(Player1.getSpeedX(), Player2.getSpeedX()));
				}
				else if(Player1.getSpeedX() < -Player2.getSpeedX()){
					Player1.moveX(pushMovement(Player2.getSpeedX(), Player1.getSpeedX()));
				}
				else{
					Player1.moveX(Player2.getSpeedX());
					Player2.moveX(Player1.getSpeedX());
				}
			}
			else{
				if(-Player1.getSpeedX() > Player2.getSpeedX()){
					Player2.moveX(pushMovement(Player1.getSpeedX(), Player2.getSpeedX()));
				}
				else if(-Player1.getSpeedX() < Player2.getSpeedX()){
					Player1.moveX(pushMovement(Player2.getSpeedX(), Player1.getSpeedX()));
				}
				else{
					Player1.moveX(Player2.getSpeedX());
					Player2.moveX(Player1.getSpeedX());
				}
			}
		}
	}

	/**
	 * A determination is made in case P1 and P2 almost completely overlap.
	 * @param Player1
	 * @param Player2
	 */
	private void detectionFusion(SimCharacter Player1, SimCharacter Player2){
		// whether P1 and P2 overlap (collide) or not?
		if( Player1.getHitAreaL() < Player2.getHitAreaR() && Player1.getHitAreaT() < Player2.getHitAreaB() && Player1.getHitAreaR() > Player2.getHitAreaL() && Player1.getHitAreaB() > Player2.getHitAreaT()){
			// if first player is left
			if((Player1.getHitAreaL() + (Player1.getHitAreaR() - Player1.getHitAreaL()) / 2) 
					< (Player2.getHitAreaL() + (Player2.getHitAreaR()-Player2.getHitAreaL()) / 2)){
				Player1.moveX(-2);
				Player2.moveX(2);
			// if second player is left 
			}else if((Player1.getHitAreaL() + (Player1.getHitAreaR() - Player1.getHitAreaL()) / 2) 
					> (Player2.getHitAreaL() + (Player2.getHitAreaR()-Player2.getHitAreaL()) / 2)){
				Player1.moveX(2);
				Player2.moveX(-2);
			}else{
				if(Player1.isFront()){
					Player1.moveX(-2);
					Player2.moveX(2);
				}
				else{
					Player1.moveX(2);
					Player2.moveX(-2);
				}
			}
		}
	}
	
	/**
	 * Effect when both characters are in an edge of the stage.
	 */
	private void decisionEndStage(){
		// if action is down, the character will be rebounded.
		// first player's effect
		if(playerOneCharacter.getHitAreaR()>stageMaxX){

			if(playerOneCharacter.getAction() == Action.DOWN){
				playerOneCharacter.reversalSpeedX();
			}
			playerOneCharacter.moveX(-playerOneCharacter.getHitAreaR()+stageMaxX);
		}
		else if(playerOneCharacter.getHitAreaL() < 0){
			if(playerOneCharacter.getAction() == Action.DOWN){
				playerOneCharacter.reversalSpeedX();
			}
			playerOneCharacter.moveX(-playerOneCharacter.getHitAreaL());			
		}
		// second player's effect
		if(playerTwoCharacter.getHitAreaR()>stageMaxX){
			if(playerTwoCharacter.getAction() == Action.DOWN){
				playerTwoCharacter.reversalSpeedX();
			}
			playerTwoCharacter.moveX(-playerTwoCharacter.getHitAreaR()+stageMaxX);
		}
		else if(playerTwoCharacter.getHitAreaL() < 0){
			if(playerTwoCharacter.getAction() == Action.DOWN){
				playerTwoCharacter.reversalSpeedX();
			}
			playerTwoCharacter.moveX(-playerTwoCharacter.getHitAreaL());			
		}
	}
	
	/**
	 * Calculate the amount of movement
	 * @param P1spd
	 * @param P2spd
	 * @return The amount of movement
	 */
	private int pushMovement(int P1spd, int P2spd){
		return (P1spd-P2spd);	
	}	
}
