package simulator;

import java.util.Deque;

import structs.FrameData;
import structs.GameData;
import enumerate.Action;

public class Simulator {
	
	private GameData gameData;

	public Simulator(GameData gameData){
		this.gameData = gameData;
	}
	
	/*
	 * Simulation function for AI.
	 * This method will take in the current frameData, actions to be performed and the number of frames to be simulated.
	 * The user can load a series of actions to be performed in the simulation. An action will be performed once the simulated player is able to act. 
	 * The resulting frame will be returned to the AI, which can access the final score and other information in the returned frame.
	 * 
	 * Note that when the character is on ground, all AIR actions will be considered invalid by the simulator (all GROUND actions are considered invalid if the character is in air as well).
	 * To simulate an air action when the character is initially on ground, add Action.JUMP to the action list before the AIR action.
	 * i.e. myAct.add(Action.JUMP); myAct.add(Action.AIR_A);
	 * 
	 * Note that if you set huge simulationLimit value, there is possibility that you are not able to conduct a simulation.
	 */
	public FrameData simulate(FrameData frameData, boolean player, Deque<Action> myAct, Deque<Action> oppAct, int simulationLimit){

		int limit = simulationLimit;
		
		SimFighting simFighting = new SimFighting(gameData, new FrameData(frameData), player, myAct, oppAct);
		
		for(int i = 0 ; i < limit ; i++){
			simFighting.processingFight();
		}

		return simFighting.getFrameData(frameData.getRemainingTime() - limit, null) ;
	}

}