import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.String;
import java.util.Random;
import java.util.Scanner;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import structs.CharacterData;
import gameInterface.AIInterface;
import enumerate.Action;
import enumerate.State;
import commandcenter.CommandCenter;

public class WengerGA9 implements AIInterface {

	Key inputKey;
	FrameData frameData;
	GameData gd;
	CommandCenter cc;
	Action OppAction;
	Action OppPreAction;
	Random rnd;
	CharacterData MyCharacter;
	CharacterData OppCharacter;

	// CharacterData
	int CurrentRound;
	int OpptoMydisX;
	//int OpptoMydisY;
	String MyChaState;
	String OppChaState;
	boolean playerNumber;

	// defaultData
	int defaultlongDisX = 400;
	int defaultshortDisX = 75;
//	int defaultGroundDisY = 100;
//	int defaultAirDisY = 800;
	int actionSize = 31;
	int costEnergyAtt=14;
	// GA value
	String actionName[] = new String[actionSize];
	int requiredPower[] = new int[actionSize];
	int requiredDisX[] = new int[actionSize];
	int requiredDisY[] = new int[actionSize];
	double actionProb[] = new double[actionSize];

	@Override // void Start in unity
	public int initialize(GameData gameData, boolean playerNumber) {
		gd = gameData;
		frameData = new FrameData();
		CurrentRound = 0;
		this.playerNumber = playerNumber;// check whether player is P1
		inputKey = new Key();// initializes a Key instance.
		OppAction = Action.NEUTRAL;
		OppPreAction = Action.NEUTRAL;
		rnd = new Random();// initializes a random instance.
		cc = new CommandCenter();

		// Add here a code to read data from file to determine the value for
		// action rank

		try (BufferedReader br = new BufferedReader(new FileReader("AttackChromosome9.txt"))) {
			System.out.println("Reading");
			String line;

			int i = 0;
			while ((line = br.readLine()) != null) {
				actionName[i] = line;
				if (line.equals("STAND_D_DF_FC")) {
					requiredPower[i] = 150;
					requiredDisX[i] = defaultlongDisX;
//					requiredDisY[i] = defaultGroundDisY;
				} else if (line.equals("AIR _D_DB_BB") || line.equals("AIR _D_DF_FB")) {
					requiredPower[i] = 50;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("STAND_D_DB_BB") || line.equals("STAND_F_D_DFB")) {
					requiredPower[i] = 50;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("AIR _F_D_DFB")) {
					requiredPower[i] = 40;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("STAND_D_DF_FB")) {
					requiredPower[i] = 30;
					requiredDisX[i] = defaultshortDisX;// need to change
				} else if (line.equals("THROW_B")) {
					requiredPower[i] = 20;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("AIR _D_DB_BA") || line.equals("AIR _F_D_DFA")) {
					requiredPower[i] = 10;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("AIR _DB") || line.equals("AIR _DA")) {
					requiredPower[i] = 5;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("THROW_A")) {
					requiredPower[i] = 5;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("STAND_D_DF_FA")) {
					requiredPower[i] = 2;
					requiredDisX[i] = defaultshortDisX;
				} else if (line.equals("STAND_A") || line.equals("STAND_B") || line.equals("CROUCH_A")
						|| line.equals("CROUCH_B") || line.equals("STAND_FA") || line.equals("STAND_FB")
						|| line.equals("CROUCH_FA") || line.equals("CROUCH_FB") || line.equals("STAND_F_D_DFA")
						|| line.equals("STAND_D_DB_BA")) {
					requiredPower[i] = 0;
					requiredDisX[i] = defaultshortDisX;
				} else {
					requiredPower[i] = 0;
					requiredDisX[i] = defaultshortDisX;
				}
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// export 0-9 10 different AI.jar file
		// Add here a code to read data from file and get the double value for
		// action prob
		Scanner scan;
		File file = new File("ProbChromosome9.txt");
		try {
			scan = new Scanner(file);
			int i = 0;
			while (scan.hasNextDouble()) {
				actionProb[i] = scan.nextDouble();
				i++;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return 0;
	}

	@Override
	public void getInformation(FrameData frameData) {
		this.frameData = frameData;
		cc.setFrameData(this.frameData, playerNumber);
		CurrentRound = this.frameData.getRound();

		this.frameData = frameData;
	}

	@Override
	public Key input() {
		// returns Key
		return inputKey;
	}

	@Override
	public void processing() {

		if (!frameData.getEmptyFlag() && frameData.getRemainingTimeMilliseconds() > 0) {

			if (cc.getSkillFlag()) {
				inputKey = cc.getSkillKey();
			} else {
				cc.setFrameData(frameData, playerNumber);
				MyCharacter = cc.getMyCharacter();
				OppCharacter = cc.getEnemyCharacter();
				OpptoMydisX = (MyCharacter.getLeft() + MyCharacter.getRight()) / 2
						- (OppCharacter.getLeft() + OppCharacter.getRight()) / 2;
//				OpptoMydisY = (MyCharacter.getTop() + MyCharacter.getBottom()) / 2
//						- (OppCharacter.getTop() + OppCharacter.getBottom()) / 2;
				
				OpptoMydisX=Math.abs(OpptoMydisX);
				
				int xDifference = MyCharacter.left - OppCharacter.left;

				if ((OppCharacter.energy >= 150) && ((MyCharacter.hp - MyCharacter.hp) <= 300))
				{
					cc.commandCall("FOR_JUMP");
				}
				else if (!MyCharacter.state.equals(State.AIR) && !MyCharacter.state.equals(State.DOWN)) { 
					
					if ((OpptoMydisX > 150)) {
						cc.commandCall("FOR_JUMP"); 
					} else if (MyCharacter.energy >= 150) {
						cc.commandCall("STAND_D_DF_FC"); 
					}else if ((OpptoMydisX > 100) && (MyCharacter.energy >= 50)) {
						cc.commandCall("STAND_D_DB_BB"); 
					} else if (OppCharacter.state.equals(State.AIR)) {
						cc.commandCall("STAND_F_D_DFA"); 
					} else if (OpptoMydisX > 100) {
						gaResult("null");
					} else {
						gaResult("null");
					}
				} else if ((OpptoMydisX <= 150)
						&& (MyCharacter.state.equals(State.AIR) || MyCharacter.state.equals(State.DOWN))
						&& (((gd.getStageXMax() - (MyCharacter.left + MyCharacter.right) / 2) >= 200)
								|| (xDifference > 0))
						&& (((MyCharacter.left + MyCharacter.right) / 2 >= 200) || xDifference < 0)) { 
					if (MyCharacter.energy >= 2) {
						gaResult("Cost Energy");//A parameter only run cost energy actions						
					} else {
						gaResult("Non-Cost Energy");
						//B only run non-cost energy actions
						}
				} else {
					// all choice, no limit
					gaResult("null");
				}
			}
		}
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	public void gaResult(String attCase) {
		switch(attCase){
		case "Cost Energy":
			for(int i=0;i<costEnergyAtt;i++){
				if (OpptoMydisX < requiredDisX[i] &&cc.getMyEnergy() >= requiredPower[i] && randomDouble() <= actionProb[i]) {
				cc.commandCall(actionName[i]);
				break;
				}
			}
			break;
		
		case "Non-Cost Energy":
			for(int i=costEnergyAtt;i<actionSize;i++){
				if (OpptoMydisX < requiredDisX[i] &&cc.getMyEnergy() >= requiredPower[i] && randomDouble() <= actionProb[i]) {
				cc.commandCall(actionName[i]);
				break;
				}
			}
			break;
			
			default:

				for(int i=0;i<actionSize;i++){
					if (OpptoMydisX < requiredDisX[i] &&cc.getMyEnergy() >= requiredPower[i] && randomDouble() <= actionProb[i]) {
					cc.commandCall(actionName[i]);
					break;
					}
				}
				break;
		}
	}

	double randomDouble() {
		Random r = new Random();
		return r.nextInt(1000) / 1000.0;
	}

	public String getCharacter() {
		return CHARACTER_ZEN;
	}
}